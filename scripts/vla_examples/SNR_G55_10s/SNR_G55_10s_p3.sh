#!/bin/bash
# Run this in the directory containing SNR_G55_10s.calib.ms
python $RASCIL/rascil/apps/rascil_imager.py --mode cip \
--ingest_msname SNR_G55_10s.calib.ms --ingest_dd 0 1 2 3 --ingest_vis_nchan 64 \
--ingest_chan_per_blockvis 8 --ingest_average_blockvis True \
--imaging_npixel 2048 --imaging_cellsize 3.878509448876288e-05 \
--imaging_weighting robust --imaging_robustness -0.5 \
--clean_nmajor 10 --clean_algorithm mmclean --clean_scales 0 6 10 --clean_gain 0.1 \
--clean_fractional_threshold 0.5 --clean_threshold 0.12e-3 --clean_nmoment 3 \
--clean_facets 8 --clean_overlap 64 --clean_psf_support 640 --clean_restored_output integrated
