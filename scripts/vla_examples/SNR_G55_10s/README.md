
# RASCIL script for SNR_G55_10s continuum imaging pipeline

This script runs the RASCIL continuum imaging pipeline on an observation from the VLA D-configuration.

The MS can be loaded from http://casa.nrao.edu/Data/EVLA/SNRG55/SNR_G55_10s.calib.tar.gz

See https://casaguides.nrao.edu/index.php?title=VLA_CASA_Imaging-CASA5.7.0 for the CASA steps

Background on the script:

- Note that the weights are very large (~10^8), which can uncover problems such as in robust weighting.
- The frequency coverage is broad and quite a bit of interference is present especially around mid 
  band. Hence the sensitivity varies across the band.

SNR_G55_10s.sh run be run on a 2019 16in MacBook pro with 8 cores and 64GB RAM. It makes a 2048
pixel image and in the CLEAN deconvolution uses 8 by 8 facets overlapping by 64 pixels.

SNR_G55_10s_p3.sh run be run on Alaska/P3 using the slurm script. It makes a smaller image 
(1280 pixels) and does not use overlapping in the CLEAN deconvolution.

