#!/bin/bash
# Run this in the directory containing sis14_twhya_calibrated_flagged.ms
python $RASCIL/rascil/apps/rascil_imager.py --mode cip \
--ingest_msname sis14_twhya_calibrated_flagged.ms --ingest_dd 0 --ingest_vis_nchan 384 \
--ingest_chan_per_blockvis 48 --imaging_nchan 48 \
--imaging_npixel 512 --imaging_cellsize 1.212034e-06 --imaging_weighting natural \
--clean_algorithm mmclean --clean_threshold 0.0 \
--clean_facets 1 --clean_overlap 0 --clean_psf_support 256 \
--clean_scales 0 6 10 --clean_nmoment 3 --clean_fractional_threshold 0.5
