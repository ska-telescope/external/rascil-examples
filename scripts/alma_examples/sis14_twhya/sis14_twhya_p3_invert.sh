#!/bin/bash
# Run this in the directory containing sis14_twhya_calibrated_flagged.ms
python $RASCIL/rascil/apps/rascil_imager.py --mode invert \
--ingest_msname sis14_twhya_calibrated_flagged.ms --ingest_dd 0 --ingest_vis_nchan 384 \
--ingest_chan_per_blockvis 48 --imaging_nchan 48 \
--imaging_npixel 512 --imaging_cellsize 1.212034e-06 --imaging_weighting natural
