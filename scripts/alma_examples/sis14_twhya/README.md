
The file can be downloaded from https://bulk.cv.nrao.edu/almadata/public/working/sis14_twhya.tgz

There are two scripts. 

The invert one just makes a dirty image spectral cube. 

The CIP script runs the Continuum Imaging pipeline with multi-scale multifrequency synthesis CLEAN.
