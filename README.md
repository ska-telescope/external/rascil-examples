
RASCIL Examples
===========================================================

Consists of examples with calibration and imaging for several CASA tutorial examples.
Used for SKA users to gain familiarity with different parameters and the continuum imagine pipeline.

Each directory contains a tutorial (in README.md) and a set of scripts to demonstrate the difference with different parser arguments used. 

For questions, consult Tim Cornwall or Celeste Lu.

